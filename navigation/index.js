import React from 'react'
import { createStackNavigator, createAppContainer } from 'react-navigation'
import HomeScreen from '../screens/HomeScreen'
import AddTaskScreen from '../screens/AddTaskScreen'
import AddBuyScreen from '../screens/AddBuyScreen'

const StackNav = createStackNavigator(
	{
		Home: {
			screen: HomeScreen
		},
		AddTask: {
			screen: AddTaskScreen
		},
		AddBuy: {
			screen: AddBuyScreen
		}
	},
	{
		mode: 'modal'
	}
)

const RootNavigator = createAppContainer(StackNav)

export default RootNavigator
